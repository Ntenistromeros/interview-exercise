<?php

class ControllerXmApiYhFinance extends Controller {
    const ACCEPTABLE_CONFIGS = ['config_yh_finance_url','config_yh_finance_sub_folder','config_yh_finance_api_version','config_yh_finance_endpoint','config_yh_finance_key'];
    private $config_yh_finance_url  = "https://yh-finance.p.rapidapi.com";
    private $config_yh_finance_sub_folder = "stock";
    private $config_yh_finance_api_version  = "v3";
    private $config_yh_finance_endpoint  = "get-historical-data";
    private $config_yh_finance_key  = "6b3da8da64msh3a8457ebc8363e3p1db6ecjsn45d34ae57dee";
    private $api_result;


    public function index($request_data) {

        if(!empty($this->config->get('config_yh_finance_url'))){
            $this->config_yh_finance_url = $this->config->get('config_yh_finance_url');
        }
        if(!empty($this->config->get('config_yh_finance_sub_folder'))){
            $this->config_yh_finance_sub_folder = $this->config->get('config_yh_finance_sub_folder');
        }
        if(!empty($this->config->get('config_yh_finance_api_version'))){
            $this->config_yh_finance_api_version = $this->config->get('config_yh_finance_api_version');
        }
        if(!empty($this->config->get('config_yh_finance_endpoint'))){
            $this->config_yh_finance_endpoint = $this->config->get('config_yh_finance_endpoint');
        }
        if(!empty($this->config->get('config_yh_finance_key'))){
            $this->config_yh_finance_key = $this->config->get('config_yh_finance_key');
        }

        $url = $this->config_yh_finance_url;

        if (!empty($this->config_yh_finance_sub_folder)) {
            $url .= '/' . $this->config_yh_finance_sub_folder;
        }

        if (!empty($this->config_yh_finance_api_version)) {
            $url .= '/' . $this->config_yh_finance_api_version;
        }

        if (!empty($this->config_yh_finance_endpoint)) {
            $url .= '/' . $this->config_yh_finance_endpoint;
        }


        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );

        if (!empty($request_data['content_type'])) {
            $content_type = $request_data['content_type'];
        } else {
            $content_type = 'application/json';
        }

        // handle method and parameters
        if (isset($request_data['parameters']) && is_array($request_data['parameters']) && count($request_data['parameters'])) {
            $params = http_build_query($request_data['parameters']);
        } else {
            $params = null;
        }

        switch ($request_data['method']) {
            case 'GET' :
                $curl_options[CURLOPT_POST] = false;

                if (is_string($params)) {
                    $curl_options[CURLOPT_URL] .= ((strpos($url, '?') === false) ? '?' : '&') . $params;
                }

                break;
            case 'POST' :
                $curl_options[CURLOPT_POST] = true;

                if ($params !== null) {
                    $curl_options[CURLOPT_POSTFIELDS] = $params;
                }

                break;
            default :
                $curl_options[CURLOPT_CUSTOMREQUEST] = $request_data['method'];

                if ($params !== null) {
                    $curl_options[CURLOPT_POSTFIELDS] = $params;
                }

                break;
        }

        // handle headers
        $added_headers = array();

        $added_headers[] = 'X-RapidAPI-Host: ' . $this->config_yh_finance_url;

        if (!empty($this->api_key)) {
            $added_headers[] = 'X-RapidAPI-Key: ' .$this->config_yh_finance_key;
        }

        if (!is_array($params)) {
            // curl automatically adds Content-Type: multipart/form-data when we provide an array
            $added_headers[] = 'Content-Type: ' . $content_type;
        }

        if (isset($request_data['headers']) && is_array($request_data['headers'])) {
            $curl_options[CURLOPT_HTTPHEADER] = array_merge($added_headers, $request_data['headers']);
        } else {
            $curl_options[CURLOPT_HTTPHEADER] = $added_headers;
        }


        $this->debug("YH_FINANCE DEBUG START...");
        $this->debug("YH_FINANCE ENDPOINT: " . $curl_options[CURLOPT_URL]);
        $this->debug("YH_FINANCE HEADERS: " . print_r($curl_options[CURLOPT_HTTPHEADER], true));
        $this->debug("YH_FINANCE PARAMS: " . $params);

        // Fire off the request
        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        if ($result) {
            $this->debug("YH_FINANCE RESULT: " . $result);

            curl_close($ch);

            $json_array = json_decode($result);

            if($json_array) {
                return $json_array;
            }else{
                //Api returned message instead of a json array
                //$returned_data = $this->handle_error($result);
                return $result;
            }
        } else {
            $info = curl_getinfo($ch);

            curl_close($ch);

            //throw new \Squareup\Exception($this->registry, "CURL error. Info: " . print_r($info, true), true);
        }
    }

    /**
     *  Sets config in main class system/library/config.php
     *
     *
     *
     * @return	json message
     */
    public function update_config()
    {
        $this->load->language('xm/api/yh_finance');
        $this->load->model('xm/settings');
        $json = [];
        if(!empty($this->request->post['config_index'])){
            $config = $this->request->post['config_index'];
        }
        else{
            $config = '';
        }
        if(!empty($this->request->post['config_value'])){
            $value = $this->request->post['config_value'];
        }else{
            $value = '';
        }
        if (!empty($value) && !empty($config)  && $this->checkConfigIndex($config)) {
            $this->config->set($config,$value);
            $this->model_xm_settings->editSetting($config,$value);
            $json['success'] = true;
            $json['message'] = $this->language->get('json_success');

        }else{
            $json['error'] = true;
            $json['message'] = $this->language->get('json_error');
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function handle_error(){
        //TODO This function should be in admin side of the project. I left it here for debug purposes.
        $this->document->setTitle("Api error handling");

        $this->document->addScript('catalog/view/javascript/xm/xm.js');
        $this->load->language('xm/api/yh_finance');

        $data['footer'] = $this->load->controller('xm/footer');
        $data['header'] = $this->load->controller('xm/header');

        $data['config_yh_finance_url'] = $this->config_yh_finance_url;
        $data['config_yh_finance_sub_folder'] = $this->config_yh_finance_sub_folder;
        $data['config_yh_finance_api_version'] = $this->config_yh_finance_api_version;
        $data['config_yh_finance_endpoint'] = $this->config_yh_finance_endpoint;
        $data['config_yh_finance_key'] = $this->config_yh_finance_key;

        $data['text_config_yh_finance_url'] = $this->language->get('text_config_yh_finance_url');
        $data['text_config_yh_finance_sub_folder'] = $this->language->get('text_config_yh_finance_sub_folder');
        $data['text_config_yh_finance_api_version'] = $this->language->get('text_config_yh_finance_api_version');
        $data['text_config_yh_finance_endpoint'] = $this->language->get('text_config_yh_finance_endpoint');
        $data['text_config_yh_finance_key'] = $this->language->get('text_config_yh_finance_key');
        $data['text_responded_message'] = $this->language->get('text_responded_message');
        $data['text_changes_saved'] = $this->language->get('text_changes_saved');

        $this->response->setOutput($this->load->view('xm/api/error', $data));
    }

    /**
     *  Checks if config key belongs to this api
     *
     * @param	string	$config
     *
     * @return	bool
     */
    private function checkConfigIndex($config) :bool
    {
        if (in_array($config,self::ACCEPTABLE_CONFIGS)) {
            return true;
        }
        return false;
    }

    public function debug($text) {
        if ($this->config->get('yh_finance_debug')) {
            $this->log->write($text);
        }
    }
}