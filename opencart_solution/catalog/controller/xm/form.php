<?php

class ControllerXmForm extends Controller {

    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->document->addStyle('https://code.jquery.com/ui/1.13.2/themes/smoothness/jquery-ui.css');
        $this->document->addScript('catalog/view/javascript/jquery/jquery-ui-1-13-2.min.js');
        $this->document->addScript('catalog/view/javascript/xm/xm.js');
        $this->document->addScript('catalog/view/javascript/xm/xm_form.js');
        $this->document->addScript('catalog/view/javascript/xm/xm_fetch_company_symbols.js');

        $this->load->language('xm/form');

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['footer'] = $this->load->controller('xm/footer');
        $data['header'] = $this->load->controller('xm/header');

        $data['entry_symbol'] = $this->language->get('entry_symbol');
        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date'] = $this->language->get('entry_end_date');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['text_form'] = $this->language->get('text_form');
        $data['error_form_submit_error'] = $this->language->get('error_form_submit_error');
        $data['error_date_range'] = $this->language->get('error_date_range');
        $data['error_start_date'] = $this->language->get('error_start_date');
        $data['error_end_date'] = $this->language->get('error_end_date');
        $data['error_symbol'] = $this->language->get('error_symbol');
        $data['error_email'] = $this->language->get('error_email');

        $this->response->setOutput($this->load->view('xm/form', $data));
    }


    private function validate($data) :array
    {
        //TODO fix timezones
        $this->load->model('xm/company_symbols');
        $error = [];
        $this->load->language('xm/form');
        $now = new DateTime();
        $start_date  = new DateTime($data['start_date']);
        $end_date  = new DateTime($data['end_date']);
        if ((utf8_strlen($data['symbol']) < 1)) {
            $error['symbol'] = $this->language->get('error_symbol');
        }else{
            $symbol_exists =  $this->model_xm_company_symbols->checkCompanySymbolExists($data['symbol']);
            if(!$symbol_exists){
                $error['symbol'] = $this->language->get('error_symbol');
            }
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $error['email'] = $this->language->get('error_email');
        }

        if(empty($data['start_date']) || $now < $start_date){
            $error['start_date'] = $this->language->get('error_start_date');
        }

        if(empty($data['end_date']) || $now < $end_date ){
            $error['end_date'] = $this->language->get('error_end_date');
        }

        if($start_date > $end_date ){
            $error['date_range'] = $this->language->get('error_date_range');
        }

        // Captcha
        if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

            if ($captcha) {
                $error['captcha'] = $captcha;
            }
        }

        return $error;
    }

    public function jx_submit_form(){

        $data = [];
        $json = array();

        if(!empty($this->request->post['symbol'])){
            $data['symbol'] = $this->request->post['symbol'];
        }else{
            $data['symbol'] = '';
        }

        if(!empty($this->request->post['start_date'])){
            $data['start_date'] = $this->request->post['start_date'];
        }else{
            $data['start_date'] = '';
        }

        if(!empty($this->request->post['end_date'])){
            $data['end_date'] = $this->request->post['end_date'];
        }else{
            $data['end_date'] = '';
        }

        if(!empty($this->request->post['email'])){
            $data['email'] = $this->request->post['email'];
        }else{
            $data['email'] = '';
        }

        $json['errors'] = $this->validate($data);

        // return if errors
        if (!empty($json['errors'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }

        // post-validation
        $params = array(
            'method' => 'GET',
            'parameters' => array(
                'symbol' => $data['symbol']
            )
        );
        $data['apiResponse'] = $this->load->controller('xm/api/yh_finance',$params);
        $data['redirect'] = $this->url->link('xm/api/yh_finance/handle_error');
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    public function success() {
        $this->load->language('information/contact');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/contact')
        );

        $data['text_message'] = $this->language->get('text_message');

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $params = array(
            'symbol' => 'AMRN',
            'region'=> 'US'
        );
        $data['responseXm'] = $this->load->controller('xm/api/yh_finance/api',$params);

        $this->response->setOutput($this->load->view('common/success', $data));
    }
}