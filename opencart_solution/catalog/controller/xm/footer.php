<?php
class ControllerXmFooter extends Controller {
	public function index() {
		$this->load->language('xm/footer');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		$data['scripts'] = $this->document->getScripts('footer');
		$data['styles'] = $this->document->getStyles('footer');
		
		return $this->load->view('xm/footer', $data);
	}
}
