<?php


class ControllerXmCompanySymbols extends Controller
{
    public function index(){
        $this->load->language('xm/company_symbols');
        $json=[];
        $json['success'] = $this->sync_db();
        if($json['success']){
            $json['text'] = $this->language->get('text_successfully_fetched_names');
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * Stores fetched symbols in database
     *
     *
     * @return int Returns 0 if symbols wasn't inserted or updated. Returns 1 in case of successful insert/update
     */
    public function sync_db(): int
    {
        $this->load->model('xm/company_symbols');
        $company_symbols = $this->get_from_url();
        if($company_symbols){
            $name_index =  !empty($this->config->get('config_company_symbols_name_index')) ? $this->config->get('config_company_symbols_name_index') : 'Company Name';
            $symbol_index =  !empty($this->config->get('config_company_symbols_symbol_index')) ? $this->config->get('config_company_symbols_symbol_index') : 'Symbol';
                return $this->model_xm_company_symbols->syncCompanySymbols($company_symbols,$name_index,$symbol_index);
        }
        return 0;

    }
    /**
     * Brings symbols from url. If url is not stored in database function fetches default url
     *
     *
     * @return mixed Returns either array of company symbols or null
     */
    public function get_from_url() :mixed
    {
        if(!empty($this->config->get('config_company_symbols_url'))){
            $url = $this->config->get('config_company_symbols_url');
        }else{
            $url = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';
        }
        $company_symbols = file_get_contents($url);
        if(!empty($company_symbols)){
            return json_decode($company_symbols);
        }
        return null;
    }
    /**
     *  Reads search key from $_GET variable and returns json
     *
     *
     * @return  Returns json_encoded response with companies that match search key
     */
    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('xm/company_symbols');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start'       => 0,
                'limit'       => 10
            );

            $symbols = $this->model_xm_company_symbols->getCompanySymbols($filter_data);

            foreach ($symbols as $symbol) {
                $json[] = array(
                    'symbol' => $symbol['symbol'],
                    'name'      => strip_tags(html_entity_decode($symbol['symbol'] . ' &gt; ' . $symbol['name'] , ENT_QUOTES, 'UTF-8'))
                );
            }
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}