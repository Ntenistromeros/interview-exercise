let dateToday = new Date();
let maxStartDate = dateToday;
let minEndDate = new Date(-8640000000000000);;
let maxEndDate = dateToday;

$(document).ready(function() {
    $('input[name="symbol"]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=xm/company_symbols/autocomplete&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['symbol']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name="symbol"]').val(item['value']);
        }
    });

    $( "#start-date" ).datepicker({
        dateFormat:"yy-mm-dd",
        maxDate: maxStartDate,
        altField: "#start-date-alt",
        altFormat: "DD, d MM, yy",
    }).change(startDateChanged);
    $( "#end-date" ).datepicker({
        dateFormat:"yy-mm-dd",
        minDate: minEndDate,
        maxDate: maxEndDate,
        altField: "#start-date-alt",
        altFormat: "DD, d MM, yy"
    });
    $("#xm-submit-button").click(function() {
        var url = 'jx_submit_form';
        let has_empty_values = false;
        $('.error').html('');
        $('#error-holder').empty();
        $('.has-error').removeClass('has-error');
        if ($('#symbol').val() ==='') {
            $('#symbol').parents('.form-group').addClass('has-error');
            $('#error-holder').append(xmGlobals.symbolError+ '<BR>').removeClass('hidden');
            has_empty_values = true;
        }
        if ($('#start-date').val() ==='') {
            $('#start-date').parents('.form-group').addClass('has-error');
            $('#error-holder').append(xmGlobals.startDateError+ '<BR>').removeClass('hidden');
            has_empty_values = true;
        }
        if ($('#end-date').val() ==='') {
            $('#end-date').parents('.form-group').addClass('has-error');
            $('#error-holder').append(xmGlobals.endDateError+ '<BR>').removeClass('hidden');
            has_empty_values = true;
        }
        if ($('#email').val().length < 3) {
            $('#email').parents('.form-group').addClass('has-error');
            $('#error-holder').append(xmGlobals.emailError+ '<BR>').removeClass('hidden');
            has_empty_values = true;
        }
        if(has_empty_values === false) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: $('base').attr('href') + 'index.php?route=xm/form/' + url,
                data: $("form#symbol-request").serialize(),
                beforeSend: function () {
                    //TODO spin button
                },
                complete: function (jqXHR, textStatus) {
                    if (textStatus != 'success') {
                        $('.alert-danger').text(xmGlobals.formError).show();
                        window.scrollTo(0, 0);
                        setTimeout(function () {
                            $(".ms-spinner").button('reset');
                        }, 1000);
                    }
                },
                error: function (htmlData) {
                    $('#error-holder').removeClass('hidden');
                    $('#error-holder').empty();
                    $('#error-holder').text(xmGlobals.formError);
                },
                success: function (jsonData) {
                    if (jsonData.fail) {
                        $('.alert-danger').text(xmGlobals.formError).show();
                        window.scrollTo(0, 0);
                    } else if (!jQuery.isEmptyObject(jsonData.errors)) {
                        if (jsonData.errors) {
                            $('#error-holder').removeClass('hidden');
                            $('#error-holder').empty();
                        }
                        for (let error in jsonData.errors) {
                            var error_key = error.replace(/[\[\]*()?]/g, "\\$&");

                            $('#error-holder').append(jsonData.errors[error] + '<BR>');
                            if (!jsonData.errors.hasOwnProperty(error)) {
                                continue;
                            }

                            if ($('#' + error_key).length > 0) {
                                $('#' + error_key).parents('.form-group').addClass('has-error');
                            }

                        }
                        $('.alert-danger').show();
                        window.scrollTo(0, 0);
                    } else if (!$.isEmptyObject(jsonData.data)) {
                        console.log('data received');
                    } else {
                        console.log("showing modal");
                        $("#alert_modal > div > div > div.modal-header > h5").text('API Response');
                        $("#alert_modal .modal-body").html(jsonData.apiResponse);
                        $("#alert_modal").modal("show");
                        setTimeout(function() {
                            window.location = jsonData.redirect;
                        }, 5000);
                    }
                }
            });
        }
    });
});

function startDateChanged(){
    minEndDate = $(this).datepicker('getDate');
    $( "#end-date" ).datepicker("option", 'minDate', minEndDate);
    console.log('fixed');
}

function endDateChanged(){
    console.log();
}