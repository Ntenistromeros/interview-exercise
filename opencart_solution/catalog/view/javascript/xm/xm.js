$( document ).ready(function() {
    $('.save-on-change').change(function() {
        let config_key = $(this).attr("id");
        let config_value = $(this).val();
        if (config_value !=='' && config_key !=='') {
            $.post($('base').attr('href') + "index.php?route=xm/api/yh_finance/update_config",
                {
                    config_index: config_key,
                    config_value: config_value
                }
            ).done(function( jsonData ) {
                $('#error-holder').html();
                $('#sync_response').html();
                if(jsonData.hasOwnProperty('success')){
                    if (jsonData.hasOwnProperty('message')) {
                        $('#sync_response').append(jsonData.message + config_key).removeClass('hidden');
                    }
                }
                else{
                    if (jsonData.hasOwnProperty('message')) {
                        $('#error-holder').append(jsonData.message).removeClass('hidden');
                    }
                }
            });
        }
    });
});