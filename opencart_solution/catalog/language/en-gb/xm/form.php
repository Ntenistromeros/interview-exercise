<?php
// Text
$_['entry_symbol']      = 'Company Symbol (autocomplete)';
$_['entry_start_date']      = 'Start Date';
$_['entry_end_date']      = 'End Date';
$_['entry_email']      = 'Email: ';
$_['text_form']      = 'Select company symbol and date range to see results.';
$_['error_form_submit_error'] = 'Error occurred when submitting the form. Please contact the store owner for more information.';
$_['error_date_range'] = 'Please select a valid date range.';
$_['error_start_date'] = 'Start date must be less or equal than current date.';
$_['error_end_date'] = 'End date must be less or equal than current date.';
$_['error_symbol'] = 'Only Company Symbols from a dropdown list are accepted.';
$_['error_email'] = 'Please give us a valid email account.';
