<?php
// Text
$_['text_config_yh_finance_url']      = 'Base url: ';
$_['text_config_yh_finance_sub_folder']      = 'Sub folder: ';
$_['text_config_yh_finance_api_version']      = 'Api version: ';
$_['text_config_yh_finance_endpoint']      = 'Api Endpoint: ';
$_['text_config_yh_finance_key']      = 'Api Secret Key: ';
$_['text_responded_message']      = 'Responded message: ';
$_['text_changes_saved']      = 'All values are saved automatically, after user finishes editing (onChange): ';
$_['json_success']      = 'Successfully updated config: ';
$_['json_error']      = 'Something went wrong.';
