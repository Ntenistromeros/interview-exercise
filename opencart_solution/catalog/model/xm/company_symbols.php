<?php


class ModelXmCompanySymbols extends Model
{
    public function checkCompanySymbolExists($symbol){
            $sql = "SELECT * FROM `" . DB_PREFIX . "company_symbols` WHERE symbol='".$this->db->escape($symbol)."'";

            $query = $this->db->query($sql);

            return $query->num_rows;
    }

    public function getCompanySymbols($data){
            $sql = "SELECT * FROM `" . DB_PREFIX . "company_symbols` WHERE 1 ";

            if (!empty($data['filter_name'])) {
                $sql .= " AND (symbol LIKE '%" . $this->db->escape($data['filter_name']) . "%' or name LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
                $sql .= " ORDER BY case 
                                when symbol='".$this->db->escape($data['filter_name'])."' then 1
                                when name='".$this->db->escape($data['filter_name'])."' then 2
								when symbol LIKE '" . $this->db->escape($data['filter_name']) . "%' then 3
								when name LIKE '" . $this->db->escape($data['filter_name']) . "%' then 4
								when symbol LIKE '%" . $this->db->escape($data['filter_name']) . "%' then 5 
								when name LIKE '%" . $this->db->escape($data['filter_name']) . "%' then 6 end";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
    }

    public function syncCompanySymbols($company_symbols,$name_index,$symbol_index){
        $values =[];

        $sql = "REPLACE INTO `".DB_PREFIX."company_symbols` (`symbol`,`name`) VALUES ";

        foreach ($company_symbols as $company_symbol){
            $values[] = "('" . $this->db->escape($company_symbol->{$symbol_index}) . "','" . $this->db->escape($company_symbol->{$name_index}) . "')";

        }
        if(!empty($values)){
            $sql .= implode(",", $values);
            return $this->db->query($sql);
        }
        return 0;
    }



}