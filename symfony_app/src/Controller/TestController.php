<?php
namespace App\Controller;

use GuzzleHttp\ClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TestController extends AbstractController
{
    private HttpClientInterface $client;
    private string $api_url;
    private string $api_key;

    public function __construct(HttpClientInterface $client)
    {

        $this->client = $client;
        $config_yh_finance_key = "6b3da8da64msh3a8457ebc8363e3p1db6ecjsn45d34ae57dee";
        $this->api_url = 'https://yh-finance.p.rapidapi.com';
        $this->api_key = $config_yh_finance_key;
    }

    #[Route('/', name: 'blog_list')]
    public function list(): Response
    {
        $url = 'https://yh-finance.p.rapidapi.com/stock/v3/get-historical-data?symbol=AMRN&region=US';
        try {
            $res = $this->client->request('GET', $url, [
                'headers' => [
                    'X-RapidAPI-Key' => $this->api_key,
                    'X-RapidAPI-Host' => $this->api_url,
                    'Content-Type' => 'application/json'
                ]

            ]);
        }
        catch (TransportExceptionInterface $e) {
            dd($e);
        }
        catch (\Exception $exception){
            dd($exception);
        }
        dd($res->getContent());
    }
}